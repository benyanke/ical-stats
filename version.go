package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

// Injected at build time for things built in CI, but not for installs using 'go get'
var GitCommitHash string
var GitCommitShortHash string
var GitProject string
var GitTag string

// Gitlab releases API inserted here at buildtime
// Will be a value like: "https://gitlab.com/api/v4/projects/[project]/releases"
// Only works if the gitlab project is public
var ReleasesUrl string

// version-related functions

// Prints version info to stdout
// Three cases to handle:
//  1. all var are empty. was built using "go build" during
//     development, or installed with go get (which just runs
//
// /   go build for the end user)
//  2. All are populated except GitTag. This is built by CI, on a non-release
//  3. All are populated. This is built by CI, for a release or release candidate.
//     3a) if the tag ends in "-rcX" where x is an integer, it's a release candidate
//     3a) if the tag matches vX.X.X where x is an integer, it's a release
func printVersionInfo() {

	if GitCommitHash == "" && GitCommitShortHash == "" && GitTag == "" {

		fmt.Println("No version available with the current build.")

	} else if GitCommitHash != "" && GitCommitShortHash != "" && GitTag == "" {

		fmt.Printf("Development build on commit %s\n", GitCommitShortHash)

		// I know the GitTag check is not valid, need to look into regex checking for this
		//} else if GitCommitHash != "" && GitCommitShortHash != "" && GitProject != "" && GitTag == "*-rcX" {
		//
		//	fmt.Printf("Pre-release build version %s, on commit %s\n", GitTag, GitCommitShortHash)

	} else if GitCommitHash != "" && GitCommitShortHash != "" && GitTag != "" {

		fmt.Printf("Release build version %s, on commit %s\n", GitTag, GitCommitShortHash)

	} else {
		panic("Unexpected state in version function - please report this error")
	}

	if GitProject != "" {
		fmt.Printf("Git repo URL: %s\n", GitProject)
	}

}

// Checks for updates by querying the git repo
func updateCheck() {

	if ReleasesUrl != "" && GitTag != "" {

		releasesList, err := fetchURLContent(ReleasesUrl)
		latestRemoteVersion := parseLatestVersion(releasesList)

		if err == nil {
			if strings.TrimSpace(latestRemoteVersion) != strings.TrimSpace(GitTag) {
				fmt.Println("New version available! You're running '" + strings.TrimSpace(GitTag) + "' and '" + strings.TrimSpace(latestRemoteVersion) + "' is now available.")
			}
		}
	}

}

// Parses the gitlab releases api response and returns the git tag of the last release
func parseLatestVersion(allReleasesJson string) string {

	var releases []GitlabRelease
	json.Unmarshal([]byte(allReleasesJson), &releases)

	return releases[0].Tag

}
