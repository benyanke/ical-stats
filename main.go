package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"

	"gitlab.com/benyanke/ical-stats/config"
	"gitlab.com/benyanke/ical-stats/ical"
)

type GitlabRelease struct {
	Tag string `json:"tag_name"`
}

func main() {

	// TODO: need to put this behind a flag like -V/--version but currently putting here for debugging
	printVersionInfo()

	updateCheck()

	configuration, err := config.Bootstrap()
	if err != nil {
		fmt.Println(err)
		panic("could not setup config")
	}

	if configuration.PrintHelp {
		config.PrintUsage()
		os.Exit(0)
	}

	// TODO : add handling to ask templating of default file if file not found

	for _, c := range configuration.Calendars {

		content, err := fetchURLContent(c.Url)
		if err != nil {
			panic("unable to fetch calendar " + c.Name + " from URL " + c.Url)
		}

		events, err := ical.ParseIcal(
			content,
			configuration.LookBackDays,
			configuration.LookForwardDays,
			configuration.Timezone)

		if err != nil {
			panic("could not parse ical file")
		}

		var selectedStartDate time.Time

		if configuration.DaysOffset == 0 {
			// Default to today if offset not set
			selectedStartDate = time.Now()
		} else {
			fmt.Println("Calculating stats for " + strconv.Itoa(int(configuration.DaysOffset)) + " day(s) ago because -d is set")
			selectedStartDate = time.Now().AddDate(0, 0, -int(configuration.DaysOffset))
		}

		fmt.Printf("\nStats from calendar '%s':\n", c.Name)

		for i := range configuration.DaysToPrint {

			currentDay := selectedStartDate.AddDate(0, 0, i)

			selectedEvents := ical.GetEventsOnDate(events, currentDay)

			hourCountByCategory := ical.GetHourCountByCategory(selectedEvents)

			hoursTotal := 0.0
			outputText := ""
			for category, hours := range hourCountByCategory {
				hr := hours.Hours()
				hoursTotal += hr
				hoursStr := fmt.Sprintf("%.4g", hr) + "h"
				outputText += fmt.Sprintf("   - %-5s - %s\n", hoursStr, category)
			}

			fmt.Printf(" > %s - total: %.4gh:\n", currentDay.Format("Monday / 2006-01-02"), hoursTotal)

			fmt.Println(outputText)

			if os.Getenv("ICAL_STATS_DEBUG") == "1" {
				fmt.Println("Debug info on events:")
				for _, e := range selectedEvents {
					fmt.Println("---------")
					fmt.Println("title:    " + e.Title)
					fmt.Print("start:    ")
					fmt.Println(e.StartTime)
					fmt.Print("duration: ")
					fmt.Println(e.Duration)
					fmt.Println("category: " + e.Category)
				}
				fmt.Print("\n")
			}
		}
	}
}

// Fetches content from the URL, returns it back to return string
func fetchURLContent(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", fmt.Errorf("GET error: %v", err)
	}
	defer resp.Body.Close()

	// TODO : Retry here on retryable errors
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("status error: %v", resp.StatusCode)
	}

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("read body: %v", err)
	}

	return string(data), nil

}
