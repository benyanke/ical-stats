package config

import (
	"flag"
	"fmt"
	"os"

	"github.com/spf13/viper"
)

type Config struct {
	Calendars       []CalendarConfig `mapstructure:"calendars"`
	Timezone        string           `mapstructure:"timezone"` // defaults to the OS tz
	LookBackDays    int              `mapstructure:"cal_look_forward_days"`
	LookForwardDays int              `mapstructure:"cal_look_backward_days"`
	DaysOffset      int              `mapstructure:"days_offset"`
	DaysToPrint     int              `mapstructure:"days_to_print"`

	PrintHelp bool
}

type CalendarConfig struct {
	Name string `mapstructure:"name"`
	Url  string `mapstructure:"url"`
}

func PrintUsage() {

	flag.Usage()

}

// Bootstraps the config stack, returning a finalized config struct
func Bootstrap() (Config, error) {

	appName := "ical-stats"
	appUrl := "gitlab.com/benyanke/ical-stats"

	// Setup viper config
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	viper.AddConfigPath("$HOME/.config/" + appName)
	viper.AddConfigPath("/etc/" + appName)

	// useful for debugging, not real usage
	// TODO : add something here to fix that
	viper.AddConfigPath(".")

	var configuration Config

	// "Local" is an alias for the OS zone at runtime. Could also be things like "America/Chicago".
	viper.SetDefault("Timezone", "Local")

	// Sane defaults, can be adjusted if needed
	viper.SetDefault("cal_look_forward_days", 40)
	viper.SetDefault("cal_look_backward_days", 10)

	// 0 = today, -1 yesterday, 1 is tomorrow. etc.
	viper.SetDefault("days_offset", 0)

	// Parse the config
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			fmt.Println("Config file not found in any of the known paths.")
			fmt.Println()
			fmt.Println("Please create a config file in one of the following locations:")
			fmt.Println(" - ~/.config/" + appName + "/config.yml")
			fmt.Println(" - /etc/" + appName + "/config.yml")
			fmt.Println("\nExample configs can be found here: " + appUrl)
			return Config{}, fmt.Errorf("config file not found")
		} else {
			fmt.Printf("Error reading config file. %s", err)
			return Config{}, fmt.Errorf("can not read config file: %s", err)
		}
	}

	// Take our parsed config, put into our config struct
	_ = viper.Unmarshal(&configuration)

	// Finally read in some flags

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options]\n", os.Args[0])
		flag.PrintDefaults()
	}

	flag.IntVar(&configuration.DaysOffset, "d", 0, "Days to look backward (default 0, today)")
	flag.IntVar(&configuration.DaysToPrint, "r", 0, "Number of days to print, looks backward unless -d is specified")
	flag.BoolVar(&configuration.PrintHelp, "h", false, "Print help information")

	flag.Parse()

	// Default values for DaysOffset and DaysToPrint as they are interconnected
	// This makes -r 4 the same as -r 4 -d 3 unless -d is specified
	if configuration.DaysToPrint > 1 && configuration.DaysOffset == 0 {
		configuration.DaysOffset = configuration.DaysToPrint - 1
	} else if configuration.DaysToPrint == 0 {
		configuration.DaysToPrint = 1
	}

	return configuration, nil
}
