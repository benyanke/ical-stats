package ical

import (
	"sort"
	"strings"
	"time"

	"github.com/apognu/gocal"
)

// Simple abstraction for a single ical event
type Event struct {
	Title       string
	Description string
	StartTime   time.Time
	EndTime     time.Time
	Duration    time.Duration
	Category    string
}

// A collection of statistics about a type of event
type EventType struct {
	Title       string
	Description string
	StartTime   time.Time
	EndTime     time.Time
	Duration    time.Duration
}

// Accepts a raw string containing ical data, and returns a parsed datastructure, an array of the Event struct
// objects defined above
// Uses look*Days vars to define how far from the present moment to look for events, ignoring the rest
func ParseIcal(RawIcalFile string, lookBackwardDays int, lookForwardDays int, timezone string) ([]Event, error) {
	// TODO : get error handling here

	now := time.Now()

	// Setup for timezone conversion
	loc, err := time.LoadLocation(timezone)
	if err != nil {
		panic(err)
	}

	start := now.AddDate(0, 0, (-1 * lookBackwardDays))
	end := now.AddDate(0, 0, lookForwardDays)

	c := gocal.NewParser(strings.NewReader(RawIcalFile))
	c.Start, c.End = &start, &end
	c.Parse()

	// Parse out the 'gocal' package's datastructure into our own
	var eventReturn []Event

	for _, e := range c.Events {
		event := Event{}
		event.Title = e.Summary
		event.Description = e.Description
		event.StartTime = *e.Start
		event.EndTime = *e.End
		event.Duration = event.EndTime.Sub(event.StartTime)
		// TODO - currently, category is just the event title. Eventually, it'll have additional filtering/processing on it
		event.Category = slugify(strings.Split(e.Summary, ":")[0])

		// Convert to desired timezone before finishing
		event.StartTime = event.StartTime.In(loc)
		event.EndTime = event.EndTime.In(loc)

		eventReturn = append(eventReturn, event)

	}
	return eventReturn, nil
}

// Sums hours by the category field. Returns a map where the category
// name and the value is the sum of the hours in that category
func GetHourCountByCategory(events []Event) map[string]time.Duration {
	out := map[string]time.Duration{}

	for _, event := range events {
		out[event.Category] = AddDurations(event.Duration, out[event.Category])
	}
	return out

}

// Consistent slugification of strings
func slugify(in string) string {

	out := in

	out = strings.TrimSpace(out)

	for i := 1; i < 300; i++ {
		before := out
		out = strings.ReplaceAll(out, "  ", " ")
		after := out

		if before == after {
			break
		}
	}

	out = strings.ReplaceAll(out, " ", "-")
	out = strings.ReplaceAll(out, ".", "")
	out = strings.ReplaceAll(out, "'", "")
	out = strings.ReplaceAll(out, "_", "-")
	out = strings.ReplaceAll(out, ":", "-")

	out = strings.ToLower(out)

	for i := 1; i < 300; i++ {
		before := out

		out = strings.ReplaceAll(out, "--", "-")
		out = strings.ReplaceAll(out, "  ", " ")
		out = strings.TrimSpace(out)
		after := out

		if before == after {
			break
		}

	}

	return out
}

// Returns true if the event in question occurs on the same day as the input time
// explicitly speaking, it checks the start time, not the end time
func IsEventOnDay(e Event, checkTime time.Time) bool {

	// Get the data from the event
	startYear, startMonth, startDay := e.StartTime.Date()

	// Get data from comparison day
	checkYear, checkMonth, checkDay := checkTime.Date()

	if startYear != checkYear {
		return false
	}

	if startMonth != checkMonth {
		return false
	}

	if startDay != checkDay {
		return false
	}

	return true

}

// Accepts an array of events and returns the events in that array
// that occured on the date specified.
func GetEventsOnDate(events []Event, checkTime time.Time) []Event {

	var returnEvents []Event

	for _, event := range events {

		if IsEventOnDay(event, checkTime) {
			returnEvents = append(returnEvents, event)
		}
	}
	return returnEvents

}

// Get first event
// Does the main work for GetLastEventInSet and GetFirstEventInSet
// GetType == "first" or "last"
func GetEventInSet(events []Event, getType string) Event {

	// Validate getType and panic if invalid option is provided
	if !((getType == "first") || (getType == "last")) {
		panic("You must call GetEventInSet() with getType of 'first' or 'last'")
	}

	if len(events) == 0 {
		return Event{}
	}

	// sort and return the sorted output var selectedEvent Event
	selectedEvent := events[0]

	for _, event := range events {
		if getType == "first" && event.StartTime.Before(selectedEvent.StartTime) {
			selectedEvent = event
		} else if getType == "last" && event.EndTime.After(selectedEvent.EndTime) {
			selectedEvent = event
		}
	}
	return selectedEvent

}

// A thin wrapper around GetEventInSet for readibility, returns
// the first event in an array of events, sorted by start time/date
func GetFirstEventInSet(es []Event) Event {
	return GetEventInSet(es, "first")
}

// A thin wrapper around GetEventInSet for readibility, returns
// the last event in an array of events, sorted by end time/date
func GetLastEventInSet(es []Event) Event {
	return GetEventInSet(es, "last")
}

// Sorts a list of events by their start time/date
func SortEvents(events []Event) []Event {
	sort.Slice(events, func(i, j int) bool {
		return events[i].StartTime.Before(events[j].StartTime)
	})
	return events
}

// Because golang does not provide a way to directly sum Duration types, instead we will add two durations to a
// known time, and then calculate the new combined duration based on that known time
// TODO : add tests for this
func AddDurations(t1, t2 time.Duration) time.Duration {

	ComparisonBaseTime := time.Now()

	LaterTime := ComparisonBaseTime.Add(t1).Add(t2)

	return LaterTime.Sub(ComparisonBaseTime)

}

// See documentation for AddDurations, but "sub" instead, Takes t1, and subtracts t2 from it.
func SubDurations(t1, t2 time.Duration) time.Duration {

	ComparisonBaseTime := time.Now()

	LaterTime := ComparisonBaseTime.Add(t1).Add(-1 * t2)

	return LaterTime.Sub(ComparisonBaseTime)

}
