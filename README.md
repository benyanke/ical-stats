# ical Statistics Generator

This tool ingests an ical feed and outputs statistics based on today's events, to aid you in completing timecards.

## Installation

### Homebrew

Install with homebrew by running the following:

```shell
brew tap benyanke/tools https://gitlab.com/benyanke/homebrew-tools.git
brew install benyanke/tools/ical-stats
```

### Direct Download

[Go to the release page](https://gitlab.com/benyanke/ical-stats/-/releases), find the latest release NOT marked
"pre-release" and fetch the binary matching your platform and architecture.

### Go Get

If you are running golang 1.16 or greater, first ensure your `$GOPATH` is set up.

Ensure the following is in your `.profile`, `.bashrc` or other shell configuration.

```bash
export PATH=$PATH:$(go env GOPATH)/bin
export GOPATH=$(go env GOPATH)
```

Then, run:

```bash
go get gitlab.com/benyanke/ical-stats
```

Now, the binary `ical-stats` should be in `$GOPATH/bin/ical-stats`, and assuming you set up your path correctly,
you should be able to simply run `ical-stats` at your terminal.

To upgrade to the latest, run:

```bash
go get -u gitlab.com/benyanke/ical-stats
```

## Example Configuration

Below, you can find an example of an example configuration file.

For typical uses, place this file in `~/.config/ical-stats/config.yml`, or alternatively, `/etc/ical-stats/config.yml`.

```yaml
---

# Listing of calendars to generate reports on.
# Each calendar will have it's own report generated.
calendars:
  - name: "Calendar One"
    url: "https://example.com/mycal.ics"
  - name: "Calendar Two"
    url: "https://example.com/yourcal.ics"

```

## Usage

For usage, run the command with `-h`. You'll receive output like the following:

```bash
$ ical-stats -h
No version available with the current build.
Usage: ical-stats [options]
  -d int
        Days to look backward (default 0, today)
  -h    Print help information
  -r int
        Number of days to print (default 1)
```

## Looking at Previous Days

By default, `ical-stats` looks at today. To look at previous days,
use `-d`. To look at yesterday, append `-d 1` to the command, to look 2 days back, append `-d 2`, etc.

The following command will look at statistics 5 days ago.

```bash
$ ical-stats -d 5
```

## Looking at Multiple Days

By default, `ical-stats` only looks at one day - equivilent to `-d 1`. The `-d` flag indicates
how many days to look back, while `-r` indicates how many days to print.

For example, to print today's report, the default behavior of `ical-stats -d 0 -r 1` would suffice. This is the same
as running `ical-stats` with no arguments.

For example, to print the last 5 days of reports:

```bash
ical-stats -d 5 -r 5
```

## Fundamental Concepts

Take, for example, the following calendar, with 3 events today.

```
 - 8am-noon: "work"
 - noon-1pm: "lunch"
 - 1pm-5pm:  "work"
```

Given the above calendar, `ical-stats` will return the following output:

```bash
$ ical-stats

Stats from calendar 'My Calendar':
 > Monday / 2022-08-10 (total: 9h):
   - 8h   - work
   - 1h   - lunch
```

Now, for more advanced use - you can add additional comments after a `:` in the event title, which are not included in the grouping.

Take, for example, the following calendar, with the following events.

```
 - 8am-9am:      "client 1: doing a thing"
 - 9am-11am:     "client 2: more things"
 - 11am-11:30am: "lunch time"
 - 11:30am-noon: "client 1: a different thing"
 - noon-3pm:     "client 3"
 - 3pm-3:30:     "logging hours, finishing up"
```

The events will be summed up by their value before the `:`, like so:

```bash
$ ical-stats

Stats from calendar 'My Calendar':
 > Monday / 2022-08-10 (total: 7.5h):
  - 1.5h - client 1
  - 2h   - client 2
  - 3h   - client 3
  - 0.5h - lunch time
  - 0.5h - logging hours, finishing up
```

You can use each of these statistics to easily run the numbers for completing a timecard, depending on how you use your calendar.

## Env vars

Additional config (to be moved to flags at some point)

### ICAL_STATS_DEBUG
To see more info about the appointments being parsed, set this to `1`.
